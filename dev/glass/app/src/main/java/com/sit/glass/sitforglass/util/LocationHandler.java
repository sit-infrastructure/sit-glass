package com.sit.glass.sitforglass.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.sit.glass.data.Defect;
import com.sit.glass.data.LocationBundle;
import com.sit.glass.sitforglass.MainActivity;


/**
 * Created by Alex on 5/18/2016.
 */
public class LocationHandler implements LocationListener {
    private LocationManager locationManager;
    final Criteria criteria = new Criteria();
    Defect d;

    public LocationHandler(Context context) {
        locationManager = (LocationManager) context.getSystemService(context.LOCATION_SERVICE); // replaced 'context' to 'this'
    }

    public void requestLocationAndAssignToDefect(Defect d) {
        this.d = d;

        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setSpeedRequired(true);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_HIGH);

        locationManager.requestSingleUpdate(criteria, this, null);

    }

    @Override
    public void onLocationChanged(Location location) {
        LocationBundle lb = new LocationBundle();
        lb.setLatitude(location.getLatitude());
        lb.setLongitude(location.getLongitude());
        lb.setAltitude(location.getAltitude());

        d.setLocation(lb);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onProviderDisabled(String provider) {}
}
