package com.sit.glass.sitforglass;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.glass.view.WindowUtils;
import com.google.android.glass.widget.CardBuilder;
import com.sit.glass.data.Inspection;
import com.sit.glass.data.InspectionManager;
import com.sit.glass.sitforglass.util.DefectQueue;

import java.util.ArrayList;

/**
 * Created by Alex on 5/7/2016.
 */
public class SelectInspectionActivity extends Activity {

    ArrayList<Inspection> inspections;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);

        View card = new CardBuilder(this, CardBuilder.Layout.TEXT)
                .setText("Please select an inspection")
                        //.setFootnote(locationDescription)
                        //.setTimestamp("sending...")
                .getView();

        this.setContentView(card);
    }

    /**
     * Builds the menu that is specific to the camera previer
     * @param featureId
     * @param menu
     * @return
     */
    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {

        if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS){
            //getMenuInflater().inflate(R.menu.camera_menu, menu);

            inspections = InspectionManager.getInsepctions();
            for(int i=0; i<inspections.size(); i++) {
                menu.add(inspections.get(i).getName());
            }
            //menu.add("Skip");

            return true;
        }
        return super.onCreatePanelMenu(featureId, menu);
    }


    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS) {

            String inspection = item.getTitle().toString();

            for(int i=0; i<inspections.size(); i++)
                if (inspection.toLowerCase().equals(inspections.get(i).getName().toLowerCase())) {

                    DefectQueue.getCurrentDefect().setInspectionId(inspections.get(i).getId());

                    /**
                     * TODO: don't need to send any result. Can be nullified
                     */
                    Intent result = new Intent("INSPECTION_ID", Uri.parse(inspections.get(i).getId()));
                    setResult(RESULT_OK, result);
                    break;

                } else if (item.getTitle().toString().toLowerCase().equals("skip")) {
                    Intent result = new Intent("SKIP", Uri.parse("true"));
                    setResult(RESULT_CANCELED, result);
                        break;
                }

            finish();

            return true;
        }
        // Good practice to pass through to super if not handled
        return super.onMenuItemSelected(featureId, item);
    }


    /**
     * Take picture when the camera button is pressed
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            CreateNewDefectActivity.getInstance().finish();
        }

        return super.onKeyDown(keyCode, event);

    }
}
