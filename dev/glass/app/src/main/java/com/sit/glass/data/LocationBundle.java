package com.sit.glass.data;

import java.io.Serializable;

/**
 * Created by Alex on 2/16/2016.
 */
public class LocationBundle implements Serializable {

    double altitude=0, latitude=0, longitude=0, oXAxys=0, oYAxis=0, oZAxis=0;

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getoXAxys() {
        return oXAxys;
    }

    public void setoXAxys(double oXAxys) {
        this.oXAxys = oXAxys;
    }

    public double getoYAxis() {
        return oYAxis;
    }

    public void setoYAxis(double oYAxis) { 
        this.oYAxis = oYAxis;
    }

    public double getoZAxis() {
        return oZAxis;
    }

    public void setoZAxis(double oZAxis) {
        this.oZAxis = oZAxis;
    }

}
