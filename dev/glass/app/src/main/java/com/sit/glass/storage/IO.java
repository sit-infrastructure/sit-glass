package com.sit.glass.storage;

import android.content.Context;

import com.sit.glass.sitforglass.MainActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Alex on 2/22/2016.
 */
public class IO {


    /**
     * Saves a serializable object to a pecified path
     *
     * @param path
     * @param objToSerialize
     */
    public static void save(Context c, String filename, Object objToSerialize) {
        FileOutputStream outputStream;
        try {
            outputStream = c.openFileOutput(filename, Context.MODE_PRIVATE);
            ObjectOutputStream outputObjectStream = new ObjectOutputStream(outputStream);
            outputObjectStream.writeObject(objToSerialize);
            outputObjectStream.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads a file that contains a serialized object into an object
     *
     * @param file
     * @return loaded object
     */
    public static Object load(Context c, String filename) {
        Object s = null;

        FileInputStream inputStream;
        try {
            inputStream = c.openFileInput(filename);
            ObjectInputStream inputObjectStream = new ObjectInputStream(inputStream);
            s = inputObjectStream.readObject();
            inputObjectStream.close();
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return s;
    }

    /**
     * Deletes a file
     */
    public static boolean delete(Context c, String filename) {
        File file = new File(c.getFilesDir(), filename);
        return file.delete();
    }

    /**
     * Checks if a file exists
     */
    public static boolean exists(Context c, String filename) {
        File file = new File(c.getFilesDir(), filename);
        return file.exists();
    }
}
