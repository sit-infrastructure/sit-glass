package com.sit.glass.sitforglass;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.glass.view.WindowUtils;
import com.google.android.glass.widget.CardBuilder;
import com.google.android.glass.widget.CardScrollView;
import com.sit.glass.sitforglass.util.DefectQueue;

/**
 * An {@link Activity} showing a tuggable "Hello World!" card.
 * <p>
 * The main content view is composed of a one-card {@link CardScrollView} that provides tugging
 * feedback to the user when swipe gestures are detected.
 * If your Glassware intends to intercept swipe gestures, you should set the content view directly
 * and use a {@link com.google.android.glass.touchpad.GestureDetector}.
 * @see <a href="https://developers.google.com/glass/develop/gdk/touch">GDK Developer Guide</a>
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);

        if(DefectQueue.getInstance() == null) {
            Intent mServiceIntent = new Intent(this, DefectQueue.class);
            this.startService(mServiceIntent);
        }


        View view = new CardBuilder(this, CardBuilder.Layout.TITLE)
                .setText("")
                .addImage(R.drawable.icon_inverted)
                .getView();
        this.setContentView(view);
    }


    /**
     * Builds the menu that is specific to the camera previer
     * @param featureId
     * @param menu
     * @return
     */
    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {

        if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS){
            menu.add("Flag defect");
            menu.add("Previous inspections");
            menu.add("Show Glass ID");
            //menu.add("Exit");

            return true;
        }
        return super.onCreatePanelMenu(featureId, menu);
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS) {

            if(item.getTitle().toString().toLowerCase().equals("flag defect")) {
                Intent intent = new Intent(this, CreateNewDefectActivity.class);
                startActivity(intent);
            } else if(item.getTitle().toString().toLowerCase().equals("previous inspections")) {
                Intent intent = new Intent(this, PreviousDefectsActivity.class);
                startActivity(intent);
            } else if(item.getTitle().toString().toLowerCase().equals("show glass id")) {
                Intent intent = new Intent(this, GetGlassCodeActivity.class);
                startActivity(intent);
            } else if(item.getTitle().toString().toLowerCase().equals("exit")) {
                finish();
            }


            return true;
        }
        // Good practice to pass through to super if not handled
        return super.onMenuItemSelected(featureId, item);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}