package com.sit.glass.sitforglass;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.glass.widget.CardBuilder;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;
import com.sit.glass.data.Defect;
import com.sit.glass.sitforglass.util.DefectQueue;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PreviousDefectsActivity extends Activity {

    private List<CardBuilder> mCards;
    private CardScrollView mCardScrollView;
    private ExampleCardScrollAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createCards();

        mCardScrollView = new CardScrollView(this);
        mAdapter = new ExampleCardScrollAdapter();
        mCardScrollView.setAdapter(mAdapter);
        mCardScrollView.activate();
        setContentView(mCardScrollView);
    }

    private void createCards() {
        mCards = new ArrayList<CardBuilder>();


        LinkedList<Defect> defects = DefectQueue.getInstance().getSavedDefects();

        for(int i=0; i<defects.size(); i++) {
            System.out.println(i);
            Bitmap bitmap = BitmapFactory.decodeByteArray(defects.get(i).getImg(), 0, defects.get(i).getImg().length);

            mCards.add(new CardBuilder(this, CardBuilder.Layout.TEXT)
                    .setText(defects.get(i).getDefectDescription())
                    .addImage(bitmap)
                    .setTimestamp(defects.get(i).getDate().toString()));
        }


    }

    private class ExampleCardScrollAdapter extends CardScrollAdapter {

        @Override
        public int getPosition(Object item) {
            return mCards.indexOf(item);
        }

        @Override
        public int getCount() {
            return mCards.size();
        }

        @Override
        public Object getItem(int position) {
            return mCards.get(position);
        }

        @Override
        public int getViewTypeCount() {
            return CardBuilder.getViewTypeCount();
        }

        @Override
        public int getItemViewType(int position){
            return mCards.get(position).getItemViewType();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return mCards.get(position).getView(convertView, parent);
        }
    }

}
