package com.sit.glass.sitforglass;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;

import com.sit.glass.data.Defect;
import com.sit.glass.data.LocationBundle;
import com.sit.glass.data.InspectionManager;
import com.sit.glass.net.BackendAPI;
import com.sit.glass.sitforglass.util.DefectQueue;
import com.sit.glass.sitforglass.util.LocationHandler;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Alex on 5/15/2016.
 */
public class CreateNewDefectActivity extends Activity {

    public static final int RESPONSE_DESCRIPTION = 0;
    public static final int RESPONSE_PICTURE = 1;
    public static final int RESPONSE_INSPECTION = 2;
    public static final int RESPONSE_PREVIEW = 3;
    private boolean defectCreated = false;

    private Defect d;

    private static CreateNewDefectActivity instance;

    private static Queue<InputStep> inputSteps = new LinkedList<InputStep>();

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        instance = this;

        if(DefectQueue.getInstance() != null && DefectQueue.getInstance().getCurrentDefect() == null ) {

            d = DefectQueue.getInstance().createNewDefect();
            d.setDate(Calendar.getInstance().getTime());
            setLocation(d);

            // define the 4-step pro
            addStepTakePicture();
            addStepDefectDescription();
            addStepSelectInspection();
            addStepShowPreview();

            // in background, load inspections from the server
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while(InspectionManager.getInsepctions().size() == 0) { // send requests to get the inspections until they are received.
                        Thread t = new Thread(new Runnable() {

                            @Override
                            public void run() {
                                BackendAPI.requestAccessCode();
                                InspectionManager.loadCacheInspectionsFromServer();
                            }
                        });
                        t.start();
                        try {
                            t.sleep(5000);
                        } catch (InterruptedException e) { }
                    }
                }
            }).start();


            // in background, set the location coordinates to the defect
            setLocation(d);

            defectCreated=true;
        } else {DefectQueue.getInstance().removeCurrentDefect();}


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {

        if (resultCode == RESULT_OK) {
            String spokenText;

            switch (requestCode) {
                case RESPONSE_DESCRIPTION:
                    spokenText = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0);
                    DefectQueue.getCurrentDefect().setDefectDescription(spokenText);
                    break;
            }

        } else if (resultCode == RESULT_CANCELED) {
            /**
             * remove the current defect when the user exits at any point
             */

            DefectQueue.getInstance().removeCurrentDefect();

            /**
             * Return to the main screen of the application if the defect creatin was cancelled
             */
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (inputSteps.isEmpty())
            finish();
        else
            runNextStep();

    }

    public static void addStepTakePicture() {
        inputSteps.add(new InputStep() {
            @Override
            public void takeInput() {
                Intent intent = new Intent(instance, TakePictureActivity.class);
                //instance.startActivityForResult(intent, RESPONSE_PICTURE);
                instance.startActivity(intent);
            }
        });
    }

    public static void addStepShowPreview() {
        inputSteps.add(new InputStep() {
            @Override
            public void takeInput() {
                Intent intent = new Intent(instance, PreviewActivity.class);
                //instance.startActivityForResult(intent, RESPONSE_PREVIEW);
                instance.startActivity(intent);
            }
        });
    }

    public static void addStepDefectDescription() {
        inputSteps.add(new InputStep() {
            @Override
            public void takeInput() {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Please describe the defect...");
                instance.startActivityForResult(intent, RESPONSE_DESCRIPTION);
            }
        });
    }

    public static void addStepSelectInspection() {
        inputSteps.add(new InputStep() {
            @Override
            public void takeInput() {
                Intent intent = new Intent(instance, SelectInspectionActivity.class);
                //instance.startActivityForResult(intent, RESPONSE_INSPECTION);
                instance.startActivity(intent);
            }
        });
    }

    public static void runNextStep() {
        boolean success=false;
        while(!success && !inputSteps.isEmpty()) {
            try {
                success=false;
                inputSteps.element().takeInput();
                inputSteps.remove();
                success=true;
            } catch (Exception e) {
                e.printStackTrace();
                success=false;
            }
        }
    }

    /**
     * TODO: get the actual location
     * @param d
     */
    private void setLocation(final Defect d) {
        (new LocationHandler(this)).requestLocationAndAssignToDefect(d);
    }

    public static CreateNewDefectActivity getInstance() {
        return instance;
    }

    public interface InputStep {
        public void takeInput();
    }

}
