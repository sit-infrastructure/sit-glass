package com.sit.glass.sitforglass;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;

import com.google.android.glass.widget.CardBuilder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Alex on 5/9/2016.
 */
public class GetGlassCodeActivity extends Activity {


    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        View card = new CardBuilder(this, CardBuilder.Layout.TEXT)
                .setText("SIT Device ID:\n"+getDeviceCode(this))
                //.addImage(bitmap)
                .getView();
        this.setContentView(card);

    }

    public static String getDeviceCode(Context c) {

        WifiManager wifiMan = (WifiManager) c.getSystemService(
                Context.WIFI_SERVICE);
        WifiInfo wifiInf = wifiMan.getConnectionInfo();
        byte[] charArray = wifiInf.getMacAddress().getBytes();
        String code="";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte [] md5_bytes = md.digest(charArray);

            //convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < md5_bytes.length; i++) {
                sb.append(Integer.toString((md5_bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            code = sb.substring(0, 5).toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return code;
    }

}
