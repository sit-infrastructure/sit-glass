package com.sit.glass.sitforglass;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.glass.view.WindowUtils;
import com.google.android.glass.widget.CardBuilder;
import com.sit.glass.sitforglass.util.DefectQueue;

/**
 * Created by Alex on 5/8/2016.
 */
public class PreviewActivity extends Activity {

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);

        byte[] imgBytes = DefectQueue.getCurrentDefect().getImg();
        String defectDescription = DefectQueue.getCurrentDefect().getDefectDescription();
        Bitmap bitmap = BitmapFactory.decodeByteArray(imgBytes, 0, imgBytes.length);

        View card = new CardBuilder(this, CardBuilder.Layout.COLUMNS)
                .setText(defectDescription)
                .setTimestamp(DefectQueue.getCurrentDefect().getDate().toString()) //TODO: need to format date
                .addImage(bitmap)
                .getView();

        this.setContentView(card);

    }

    /**
     * Builds the menu that is specific to the camera previer
     * @param featureId
     * @param menu
     * @return
     */
    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {

        if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS){
            getMenuInflater().inflate(R.menu.preview_menu, menu);


            return true;
        }
        return super.onCreatePanelMenu(featureId, menu);
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS) {

            String command = item.getTitle().toString();

            if (command.toLowerCase().equals("send")) {

                DefectQueue.getInstance().saveCurrentDefect();

//                Intent result = new Intent("COMMAND", Uri.parse("SAVE"));
//                setResult(RESULT_OK, result);
                //finish();

                finish();

            } else if (command.toLowerCase().equals("retake picture")) {

                CreateNewDefectActivity.addStepTakePicture();
                CreateNewDefectActivity.addStepShowPreview();

                finish();
//                CreateNewDefectActivity.takePicture();
            } else if (command.toLowerCase().equals("edit description")) {

                CreateNewDefectActivity.addStepSelectInspection();
                CreateNewDefectActivity.addStepShowPreview();

                finish();

//                CreateNewDefectActivity.takeDefectDescription();
            } else if (command.toLowerCase().equals("change inspection")) {

                CreateNewDefectActivity.addStepSelectInspection();
                CreateNewDefectActivity.addStepShowPreview();

                finish();


//                CreateNewDefectActivity.selectInspection();
            }

            finish();

            return true;
        }
        // Good practice to pass through to super if not handled
        return super.onMenuItemSelected(featureId, item);
    }


    /**
     * Take picture when the camera button is pressed
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            CreateNewDefectActivity.getInstance().finish();
        }

        return super.onKeyDown(keyCode, event);

    }
}
