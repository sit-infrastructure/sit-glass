package com.sit.glass.data;

import com.sit.glass.data.Inspection;
import com.sit.glass.net.BackendAPI;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Alex on 2/16/2016.
 */
public class InspectionManager {

    static ArrayList<Inspection> inspections = new ArrayList<Inspection>();



    public static void loadCacheInspectionsFromServer () {
        inspections = BackendAPI.getInspections();
    }

    public static ArrayList<Inspection> getInsepctions () {
        return inspections;
    }
}
