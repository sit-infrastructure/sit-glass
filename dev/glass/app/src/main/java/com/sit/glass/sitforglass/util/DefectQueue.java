package com.sit.glass.sitforglass.util;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.sit.glass.data.Defect;
import com.sit.glass.storage.IO;
import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Alex on 5/15/2016.
 */
public class DefectQueue extends IntentService {
    private static final String FILENAME_DEFECT_LIST = "defectList.ser";

    private static LinkedList<Defect> defects;
    private static LinkedList<String> defectFilenames;

    private static DefectQueue instance;
    private static Defect currentDefect;

    ReentrantLock lock = new ReentrantLock();

    public DefectQueue() {
        super("SITDefectQueue");
        initialize();
    }
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public DefectQueue(String name) {
        super(name);
        initialize();
    }

    public void initialize() {

        Log.i("DefectQueue", "initializing");
        instance = this;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.currentThread().sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                loadDefects();
                startSubmissionLoop();  // submit any defects that were not submitted last time this service ran
            }
        }).start();
        Log.i("DefectQueue", "initializing done");
    }

    public static DefectQueue getInstance() {
        return instance;
    }

    public String t="";
    public void test() {
        t="call from Activity";
    }

    public void startSubmissionLoop() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    Log.i("DefectQueue", "loop sees: "+t);
                    lock.lock();
                    if (defects.size() > 0) {
                        for (int i = defects.size() - 1; i >= 0; i--) {
                            if(!defects.get(i).isSentToServer() && defects.get(i) != currentDefect) {
                                Defect d = defects.get(i);

                                lock.unlock();

                                Log.i("DefectQueue", "send defect");

                                boolean result = d.sendDefect(); // this operation will take some time, so we should not keep anything locked

                                lock.lock();
                                if (result) d.markAsSent();
                            }
                        }
                    }
                    lock.unlock();

                    try {
                        Thread.currentThread().sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }

    public LinkedList<Defect> getSavedDefects() {
        return defects;
    }

    public Defect createNewDefect() {
        Defect d = new Defect();
        //defects.add(d);

        currentDefect=d;

        return d;
    }

    public static Defect getCurrentDefect() {
        return currentDefect;
    }

    /**
     * Loads the list of defects from the device
     */
    private void loadDefects() {
        boolean saveDefectFilenames = false;
        if( IO.exists(instance, FILENAME_DEFECT_LIST) ) {
            defectFilenames = (LinkedList<String>) IO.load(instance, FILENAME_DEFECT_LIST);
            defects = new LinkedList<Defect>();
            for (int i=0; i<defectFilenames.size(); i++) {
                if (IO.exists(instance, defectFilenames.get(i))) {
                    Defect d = (Defect) IO.load(instance, defectFilenames.get(i));
                    defects.addLast(d);
                } else {
                    defectFilenames.remove(i);
                    saveDefectFilenames=true;
                }
            }
        } else {
            defectFilenames=new LinkedList<String>();
            defects = new LinkedList<Defect>();
            saveDefectFilenames=true;
        }

        if (saveDefectFilenames)
            saveDefectFilenames();
    }

    public void saveCurrentDefect() {
        Log.i("DefectQueue", "saveCurrentDefect called");

        if (currentDefect != null) {
            defects.addFirst(currentDefect);
            defectFilenames.addFirst(getDefectID(currentDefect));

            Log.i("DefectQueue", "saveCurrentDefect called");

            saveDefectFilenames();
            saveDefect(currentDefect);

            Log.i("DefectQueue", "saveCurrentDefect saved locally");

            currentDefect = null;
        }
    }

//    private void saveAll() {
//        saveDefectFilenames();
//        saveAllDefects();
//    }

    private void saveDefectFilenames() {
        if( IO.exists(instance, FILENAME_DEFECT_LIST) ) {
            IO.delete(instance, FILENAME_DEFECT_LIST);
        }
        IO.save(instance, FILENAME_DEFECT_LIST, defectFilenames);
    }

//    private void saveAllDefects() {
//        for (int i=0; i<defectFilenames.size(); i++) {
//            saveDefect(defects.get(i));
//        }
//    }

    private void saveDefect(Defect d) {
        if (IO.exists(instance, getDefectID(d)))
            IO.delete(instance, getDefectID(d)+".ser");
        IO.save(instance, getDefectID(d)+".ser", d);
    }

    private String getDefectID(Defect d) {
        return String.valueOf((d.getDate().getTime() + d.getInspectionId()).hashCode());
    }

    @Override
    protected void onHandleIntent(Intent intent) {}

    public void removeCurrentDefect() {
        currentDefect = null;
    }
}
