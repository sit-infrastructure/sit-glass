package com.sit.glass.data;

import java.util.ArrayList;

/**
 * Created by Alex on 2/16/2016.
 */
public class Inspection {

    String name;
    String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
