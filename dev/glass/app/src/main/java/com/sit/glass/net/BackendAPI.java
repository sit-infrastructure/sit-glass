package com.sit.glass.net;

import android.util.Base64;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.sit.glass.data.Inspection;
import com.sit.glass.sitforglass.CreateNewDefectActivity;
import com.sit.glass.sitforglass.GetGlassCodeActivity;


/**
 * Created by Alex on 4/25/2016.
 */
public class BackendAPI {

    private static final String inspectionURL = "http://54.175.40.59:5498/inspection/all";
    private static final String defectInsertionURL = "http://54.175.40.59:5498/defect/insert";
    private static String inspectorId="";
    private static final String getInspectorURL = "http://54.175.40.59:5498/inspector/glass";
    private static final String getLoginURL = "http://54.175.40.59:5498/login";
    private static final String LOGIN_USER = "alex";
    private static final String LOGIN_PASSWORD = "test";

    static String accessCode="";
    static ReentrantLock lock = new ReentrantLock();

    static BackendAPI instance;

    public static BackendAPI getInstance () {
        if (instance ==  null) instance = new BackendAPI();
        return instance;
    }

    public static void main(String[] args) {

    }

    public static ArrayList<Inspection> getInspections () {
        ArrayList<Inspection> inspections = new ArrayList<Inspection>();

        lock.lock();

        String content = getJSON(inspectionURL, "{\"json\":\"true\", \"user\":\""+accessCode+"\"}");

        lock.unlock();

        JsonParser parser = new JsonParser();

        System.out.println("BackendAPI" + content);

        try {
            JsonObject job = parser.parse(content).getAsJsonObject();
            //Object x = job.getAsJsonObject("results");
            JsonArray entry = job.getAsJsonObject("results").getAsJsonArray("bindings");

            int size = entry.size();

            for (int i = 0; i < size; i++) {
                String j = entry.get(i).toString();


                JsonObject insp_json = parser.parse(j).getAsJsonObject();

                String inspection_id = insp_json.getAsJsonObject("inspection").getAsJsonObject().get("value").toString();
                String inspection_name = insp_json.getAsJsonObject("name").getAsJsonObject().get("value").toString();

                Inspection inspection = new Inspection();
                inspection.setId("_:" + inspection_id.substring(1, inspection_id.length() - 1)); // TODO:TOFIX
                inspection.setName(inspection_name.substring(1, inspection_name.length() - 1));

                inspections.add(inspection);
            }
        } catch (IllegalStateException e) {
            return getInspections();
        }

        return inspections;
    }

    public static boolean sendDefect (String inspectionId, String inspectionDescription, Date date, int priority, double lat, double lng, double alt, boolean isDraft, byte[] img) {
        // need to add image, altitude, orientation coordinates

        lock.lock();

        String imgBase64 = Base64.encodeToString(img, Base64.NO_WRAP);

        /**
         * TODO: to remove the following line:
         */
        //if (inspectionId == null)

        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(tz);

        String inspectorId=getInspectorId();
        if(!inspectorId.substring(0,2).equals("_:"))
            inspectorId="_:"+inspectorId;

        System.out.println("{\"inspector\":\""+inspectorId+"\","+
                "\"inspection\":\""+inspectionId+"\","+
                "\"description\":\""+inspectionDescription+"\","+
                "\"date\":\""+df.format(date)+"\","+
                "\"priority\":\""+priority+"\","+
                "\"latitude\":\""+lat+"\","+
                "\"longitude\":\""+lng+"\","+
                "\"isDraft\":\""+isDraft+"\","+
                "\"json\":\"true\","+
                "\"image\":\""+imgBase64.substring(0,100)+"\","+
                "\"user\":\""+accessCode+"\""+
                "}");
        String result = getJSON(defectInsertionURL, "{\"inspector\":\""+inspectorId+"\","+
                "\"inspection\":\""+inspectionId+"\","+
                "\"description\":\""+inspectionDescription+"\","+
                "\"date\":\""+df.format(date)+"\","+
                "\"priority\":\""+priority+"\","+
                "\"latitude\":\""+lat+"\","+
                "\"longitude\":\""+lng+"\","+
                "\"isDraft\":\""+isDraft+"\","+
                "\"json\":\"true\","+
                "\"image\":\""+imgBase64+"\","+
                "\"user\":\""+accessCode+"\""+
                "}");

        lock.unlock();

        System.out.println(result);

        /**
         * TODO: see if the server responded with positive answer
         */
        return true;
    }



    public static String getInspectorId () {

        String glassId = "GLASSPAIRID"; // TODO: clean up the initial assignment
        /**
         * TODO: Take the glassId from the product id
         */
        glassId = GetGlassCodeActivity.getDeviceCode(CreateNewDefectActivity.getInstance());

        if (inspectorId.equals("")) {

            String content = getJSON(getInspectorURL, "{\"glass_id\":\""+glassId+"\", \"user\":\""+accessCode+"\"}");
            Log.i("BACKENDAPI", content);
            JsonParser parser = new JsonParser();

            JsonObject job = parser.parse(content).getAsJsonObject();
            //Object x = job.getAsJsonObject("results");
            inspectorId = job.getAsJsonPrimitive("inspector_id").getAsString();

            /**
             * TODO: what to happen if the inspector is not paired to the device ID
             * **/
            Log.i("inspectorID", inspectorId+"");

        }
        return inspectorId;
    }

    public static String requestAccessCode() {
        lock.lock();
        String result = getJSON(getLoginURL, "{" +
                "\"username\":\"" + LOGIN_USER + "\"," +
                "\"password\":\""+ LOGIN_PASSWORD +"\"," +
                "\"json\":\"true\"}");

        Log.i("Login: ", result);

        JsonParser parser = new JsonParser();
        String session_key = parser.parse(result).getAsJsonObject().get("session_key").getAsString();

        Log.i("Session Key: ", session_key);

        accessCode=session_key;

        lock.unlock();

        return session_key;
    }

    public static String getJSON(final String url, final String content) {
        class Result  {
            volatile String result="";
            public void update (String s) {result=s;}
            public String toString() {return result;}

        }
        final Result result = new Result();

        Thread t = new Thread(new Runnable() {

            @Override
            public void run() {

                HttpURLConnection c = null;
                try {
                    URL u = new URL(url);
                    c = (HttpURLConnection) u.openConnection();
                    c.setDoOutput(true);
                    c.setDoInput(true);
                    c.setRequestProperty("Content-Type", "application/json");
                    c.setRequestProperty("Accept", "application/json");
                    c.setRequestMethod("POST");
                    //c.setRequestProperty("Content-length", "0");
                    //c.setUseCaches(false);
                    //c.setAllowUserInteraction(false);
                    //c.setConnectTimeout(timeout);
                    //c.setReadTimeout(timeout);
                    c.getOutputStream().write(content.getBytes());
                    c.connect();



                    int status = c.getResponseCode();
                    Log.i("BackendAPI:184", ""+status);
                    Log.i("BackendAPI:158", String.valueOf(getInspectorURL));
                    Log.i("BackendAPI:159", String.valueOf(status));
                    switch (status) {
                        case 200:
                        case 201:
                            BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                            StringBuilder sb = new StringBuilder();
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line+"\n");
                            }
                            br.close();
                            result.update(sb.toString());
                    }

                } catch (MalformedURLException ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                } finally {
                    if (c != null) {
                        try {
                            c.disconnect();
                        } catch (Exception ex) {
                            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        });

        try {
            t.start();
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return result.toString();
    }
}