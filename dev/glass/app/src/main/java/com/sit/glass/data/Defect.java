package com.sit.glass.data;

import android.util.Base64;
import android.util.Log;

import com.sit.glass.net.BackendAPI;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Alex on 2/16/2016.
 */
public class Defect implements Serializable {

    String inspectionId;
    LocationBundle location;
    String defectDescription;
    Date date;
    byte[] imgJpeg;
    Inspection inspection;

    private boolean sentToServer = false;

    public void setInspectionId(String inspectionId) {this.inspectionId = inspectionId;}
    public String getInspectionId() {return this.inspectionId;}

    public LocationBundle getLocation() {
        return location;
    }

    public void setLocation(LocationBundle location) {
        this.location = location;
    }

    public String getDefectDescription() {
        return defectDescription;
    }

    public void setDefectDescription(String defectDescription) {
        this.defectDescription = defectDescription;
    }

    public byte[] getImg() {
        return imgJpeg;
    }

    public String getImgBase64() {return Base64.encodeToString(imgJpeg, Base64.NO_WRAP);}

    public void setImg(byte[] img) { this.imgJpeg = img; }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void markAsSent() {this.sentToServer=true;}
    public boolean isSentToServer() {return this.sentToServer;}

    public boolean sendDefect () {
        Log.i("Defect", ""+inspectionId);
        Log.i("Defect", ""+location.getLatitude());
        Log.i("Defect", ""+location.getLongitude());
        Log.i("Defect", ""+location.getAltitude());
        Log.i("Defect", "" + imgJpeg.length);
        Log.i("Defect", "" + Calendar.getInstance().getTime());

        this.markAsSent();

        return BackendAPI.getInstance().sendDefect(inspectionId, defectDescription, date, 0, location.getLatitude(), location.getLongitude(), location.getAltitude(), true, imgJpeg);

        /**
         * TODO: call markAsSent if the the server received the defect.
         */

    }
}
