package com.sit.glass.sitforglass;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

public class CameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private static Camera camera;
    private Camera.PictureCallback pictureCallback;

    @SuppressWarnings("deprecation")
    public CameraSurfaceView(Context context) {
        super(context);

        final SurfaceHolder surfaceHolder = this.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

    }



    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (camera != null)
            releaseCamera();

        initializeCamera(holder);
    }

    public void initializeCamera(SurfaceHolder holder) {
        camera = Camera.open();
        Log.i("CameraSurfaceView", "Camera opened.");
        // Show the Camera display
        try {
            camera.setPreviewDisplay(holder);
        } catch (IOException e) {
            this.releaseCamera();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        //if (camera == null) initializeCamera(holder);

        // Start the preview for surfaceChanged
        if (camera != null) {
            camera.startPreview();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // Do not hold the camera during surfaceDestroyed - view should be gone
        this.releaseCamera();
    }

    /**
     * Release the camera from use
     */
    public static void releaseCamera() {
        if (camera != null) {
            Log.i("Camera", "released");
            camera.release();
            camera = null;
        }
    }

    /**
     * Provide the camera object outside of this class
     */
    public void takePicture() {
        camera.takePicture(null, null, pictureCallback);
    }

    /**
     * Setting up a picture callback
     */

    public void setPictureCallback(Camera.PictureCallback pictureCallback) {
        this.pictureCallback = pictureCallback;
    }

}