package com.sit.glass.sitforglass;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;

import com.google.android.glass.view.WindowUtils;
import com.sit.glass.sitforglass.util.DefectQueue;

/**
 * Created by Alex on 5/7/2016.
 */
public class TakePictureActivity extends Activity {

    private CameraSurfaceView cameraView;
    private TakePictureActivity instance;
    private GestureDetector gestureDetector;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS);

        instance = this;


        // Initiate CameraView
        cameraView = new CameraSurfaceView(this);
        Log.i("TakePictureActivity", "started intent");

        cameraView.setPictureCallback(new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                /**
                 * add the image to the current defect being logged
                 */
                DefectQueue.getCurrentDefect().setImg(data);

                /**
                 * Build a response containing the image in BASE 64
                 * TODO: may need to be sent a null result.
                 */
                Log.i("TakePictureActivity", "picture taken");
//                Intent result = new Intent("IMAGE_BASE64", Uri.parse(""));
//                instance.setResult(RESULT_OK, result);

                /**
                 * Return to the caller
                 */
                instance.finish();

            }
        });



        // Set the view
        this.setContentView(cameraView);
    }

    /**
     * Builds the menu that is specific to the camera previer
     * @param featureId
     * @param menu
     * @return
     */
    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {

        if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS){
            getMenuInflater().inflate(R.menu.camera_menu, menu);
            return true;
        }
        return super.onCreatePanelMenu(featureId, menu);
    }


    /**
     * Take picture when the camera button is pressed
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_CAMERA) {
            // Stop the preview and release the camera.
            // Execute your logic as quickly as possible
            // so the capture happens quickly.

            cameraView.takePicture();

            return false;
        } else if(keyCode == KeyEvent.KEYCODE_BACK) {
            CreateNewDefectActivity.getInstance().finish();
        }

        return super.onKeyDown(keyCode, event);

    }


    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        if (featureId == WindowUtils.FEATURE_VOICE_COMMANDS) {

            if (item.getTitle().toString().toLowerCase().equals("take picture")){
                cameraView.takePicture();
            } else if (item.getTitle().toString().toLowerCase().equals("skip picture")) {
//                Intent result = new Intent("IMAGE_BASE64", Uri.parse("img64"));
//                setResult(RESULT_CANCELED, result);
                setResult(RESULT_CANCELED, null);
                finish();
            }

            return true;
        }
        // Good practice to pass through to super if not handled
        return super.onMenuItemSelected(featureId, item);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        cameraView.takePicture();
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Do not hold the camera during onPause
        if (cameraView != null) {
            cameraView.releaseCamera();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();

        // Do not hold the camera during onPause
        if (cameraView != null) {
            cameraView.releaseCamera();
        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Do not hold the camera during onPause
        if (cameraView != null) {
            cameraView.releaseCamera();
        }
    }


}
